import sqlite3, operator, re
import trueskill as ts
import itertools
import math
import sys
from os.path import isfile

def win_probability(team1, team2):
    env = ts.global_env()
    delta_mu = sum(r.mu for r in team1) - sum(r.mu for r in team2)
    sum_sigma = sum(r.sigma ** 2 for r in team1)
    sum_sigma += sum(r.sigma ** 2 for r in team2)
    #sum_sigma = sum(r.sigma ** 2 for r in itertools.chain(team1, team2))
    size = len(team1) + len(team2)
    denom = math.sqrt(size * (env.beta * env.beta) + sum_sigma)
    return env.cdf(delta_mu / denom)

print(sys.argv)
if(len(sys.argv)<9):
    print("not enough arguements")
    exit()

team1 = []
team2 = []

ts.setup(draw_probability=0.01)
dbexists = isfile("realdbbackup.sqlite3")
conn = sqlite3.connect("realdbbackup.sqlite3")
conn.row_factory = sqlite3.Row
c = conn.cursor()

channel_id = "722026817573224478"
for i in range(1,9):
    user_id = sys.argv[i]
    c.execute("SELECT nick, rank, sigma FROM channel_players WHERE user_id = ? AND channel_id = ?", (user_id, channel_id))

    l = c.fetchone()
    print(l[0] + " " +str(i<5))
    if i<5:
        team1.append(ts.Rating(mu=l[1],sigma=l[2]))
    else:
        team2.append(ts.Rating(mu=l[1], sigma=l[2]))

print(win_probability(team1,team2))

