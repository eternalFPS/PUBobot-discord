# The real PUBobot

This is just a fork of PUBobot. It is by no means the original. The main instance of this hosted fork of PUBobot is "PuboBloke", which is private, but you are free to host your own instance of the bot. Read up more about the original PUBobot to work out how.

https://gitlab.com/pubobot-discord/PUBobot-discord < link to original pubobot.

# Changes in this fork

There are a number of changes in this fork of pubobot.

Notably, the main one is that it uses TrueSkill instead of the basic elo system. There are some modifications/non-standard ways I am using trueskill as well, but they are very complex and only of minor importance.

It also has TWO ranking systems. An alltime ranking system, that is supposed to never be reset, as well as a seasonal leaderboard, which can be reset.

There is the report cancel feature, which the original pubobot now has as well.

There is better matchmaking, its very balanced to the point in which players complain that it is too balanced (players seemingly don't want close games, they want free wins every now and then). This is displayed at the start of a match, as "Match Quality"

There is overqueuing, which allows more than the max amount of players to be in queue (e.g. 12/8 players in queue), which is then used to make an even fairer game. (instead of first come first served, its fairest come first served. However if a player misses a game they will ALWAYS be in the next one).

Subbing is "removed" as in there are no ranked gains/losses for subbing. It displays how "balanced" the sub would be, but apart from a discord message, it doesn't actually do anything.

There is a GLOBAL leaderboard, which combines the rankings from all servers in which the bot is employeed in. This is currently configured to only display properly in my instance, so if you host your own bot expect it to have a lot of "???" characters as it doesn't know anything about the servers it is in.

Bot will also display what team it thinks will win. A "+" means it thinks that team will win. If both teams have "=" then it means it thinks its [close to] 50-50.

Ranks are changed, there is more of them and they make more sense with trueskill.

A bunch of commands are removed, as their either don't make sense with trueskill/other modifications, or are too much trouble for such little reward. An example of this is the !map command, which constantly confused players about what map they were supposed to play. !seed commands are also removed becaues they will break trueskill.

Noadds is fixed, but im not sure if it was broken in original pubobot or only broke in this instance.

When players ask for a sub, it will ping (as in DM) every player currently in queue.

Offline/AFK players are not removed from queue. (It was bugged, and we have react to play anyway so it was easier to just remove it)

There are a few more minor changes, but this is mostly everything.

# Requirements
Python 3.6+, sqlite3 module for python, discord api v1.0+. trueskil

# Running
1. Copy config.example.cfg as config.cfg and fill your discord bot token or username and password in it.
2. Run './pubobot.py' to launch the program.
3. Invite your bot to the desired server.
4. Type '!enable_pickups' on the desired channel.

# Getting help
You can find help or test the bot on the Pubobot developement server: https://discord.gg/rjNt9nC

# Credits
Developer: Leshka. You can contact me via e-mail leshkajm@ya.ru, on discord (Leshaka#8570) or pm 'Leshaka' on irc.quakenet.org server.   
Help: #warsow.pickup admin team.   
Special thanks to Mute and Perrina.

# License
Copyright (C) 2015 Leshka.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License version 3 as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See 'GNU GPLv3.txt' for GNU General Public License.
